require_relative 'recipe_recommender'
rec_rec= RecipeRecommender.new()



def mainMenu
   
puts "What would you like to do?
    1: Look up recipes from Food2Fork
    2: Export recipes 
    3: Import recipes 
    4: Count recipes 
    5: Recommendations
    6: Get recipe
    7: Quit"
end

    puts "Welcome to recipe recommender"
    while true
    mainMenu
    print ' > '
    input = gets.chomp
    break if input == "7"

    puts rec_rec.dispatch_command(input)
end