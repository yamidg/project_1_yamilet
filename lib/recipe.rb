
class Recipe
    
    attr_accessor :title, :image_url, :publisher, :publisher_url, :social_rank, :source_url, :recipe_id
    
    def initialize( information={} )
        
        self.title= information[:title]
        self.image_url= information[:image_url]
        self.publisher= information[:publisher]
        self.publisher_url= information[:publisher_url]
        self.social_rank= information[:social_rank]
        self.source_url= information[:source_url]
        self.recipe_id= information[:recipe_id]

    end
    
    def recipe_information
        if(self.title==nil || self.image_url==nil || self.publisher==nil || self.publisher_url==nil || self.social_rank==nil || self.source_url==nil || self.recipe_id==nil)
            return nil
        else
            return "Recipe: #{self.title}, Rank: #{self.social_rank}, Publisher: #{self.publisher}" 
        end
        
    end
    def to_hash
       
        new_hash= {}
        new_hash[:title]= self.title
        new_hash[:image_url]= self.image_url
        new_hash[:publisher]= self.publisher
        new_hash[:publisher_url]= self.publisher_url
        new_hash[:social_rank]= self.social_rank
        new_hash[:source_url]= self.source_url
        new_hash[:recipe_id]= self.recipe_id
        
        return new_hash
    end
    
end