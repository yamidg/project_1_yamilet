require 'json'
require 'open-uri'
require_relative 'recipe'

class RecipeRecommender
   attr_accessor :recipes_from_file, :recipes_from_food2fork
   
    def initialize(recipes1=[],recipes2=[])
       self.recipes_from_file=recipes1
       self.recipes_from_food2fork=recipes2
       
    end
    
    def dispatch_command(input)
        
        case input
            when "1"
                puts "Please enter the ingredient of the recipes that you would like to search"
                command=gets.chomp
                command=command.downcase.split(' ').join(' ')
                return food2fork_lookup(command)
            when "2"  
                puts "Please enter the name of the file"
                command=gets.chomp
                command=command.downcase.split(' ').join(' ')
                return export_recipes(command)
            when "3"
                puts "Please enter the name of the file"
                command=gets.chomp
                command=command.downcase.split(' ').join(' ')
                return import_recipes(command)
            when "4"
                return count_recipes
            when "5"
                puts "Please enter one ingredient of the recipes that you already searched from food2fork"
                command=gets.chomp
                command=command.downcase.split(' ').join(' ')
                return recommendations(command)
            when "6" 
                puts "Please enter the name of the recipe"
                command=gets.chomp
                command=command.downcase.split(' ').join(' ')
                return get_recipe(command)
            else
                puts "Unknown option"
        end
        
    end    

    def import_recipes(file_name)
        str=File.read(file_name)
        hash=JSON.parse(str)
        array=hash["recipes"]
        array.each do |recipe|  
            self.recipes_from_file<< Recipe.new(recipe)
        end
        
        
    end
    
    def export_recipes(file_name)
        
        new_array = self.recipes_from_food2fork.map(&:to_hash)
        File.write(file_name, {recipes: new_array}.to_json)
    end
   
    def recommendations(ingredient)
        new_array=[]
        str=" "
        self.recipes_from_food2fork.each do |recipe|
            str=recipe.title.downcase
            if (str.include?"#{ingredient}")
                new_array << recipe
            end   
        end
        puts "Recipes recommended:"
        puts "1. #{new_array[0].recipe_information}"
        puts "2. #{new_array[1].recipe_information}"
       
    end
    
    def count_recipes
       
        return "Recipes total: #{self.recipes_from_food2fork.count}" 
       
    end
    def get_recipe(name_of_recipe)
       recipes_results_str=" "
        self.recipes_from_food2fork.each do |r|
            str=r.title.downcase
            recipes_results_str=self.get_recipe_from_food2fork(r.recipe_id)  if (str==name_of_recipe)
        end
        
        recipes_hash=JSON.parse(recipes_results_str)
        result=recipes_hash["recipe"]["ingredients"].uniq
        result.each {|r| r}
    end
   
    def search_food2fork(ingredient)
       
        credential_str=File.read('../secrets.json')
        credential_hash=JSON.parse(credential_str)
       
        url="http://food2fork.com/api/search?key=#{credential_hash["API Key"]}&q=#{ingredient}&sort=r"
        result=open(url).read
        return result
    end
    
    def get_recipe_from_food2fork(id)
       
        credential_str=File.read('../secrets.json')
        credential_hash=JSON.parse(credential_str)
        url="http://food2fork.com/api/get?key=#{credential_hash["API Key"]}&rId=#{id}"
        result=open(url).read
        return result
    end
    #take the string from food2fork and return and array of recipes objects
    
    def import_from_food2fork(food2fork_text)
        recipes_hash=JSON.parse(food2fork_text)
        recipes_obj=[]
        recipes_obj=recipes_hash["recipes"].map do|recipe_hash|
            
            new_hash= {
                title:recipe_hash["title"],
                image_url:recipe_hash["image_url"],
                publisher:recipe_hash["publisher"],
                publisher_url:recipe_hash["publisher_url"],
                social_rank:recipe_hash["social_rank"],
                source_url:recipe_hash["source_url"],
                recipe_id:recipe_hash["recipe_id"]
            }
            
            
            Recipe.new(new_hash)
        end
        return recipes_obj
    end
    
    def food2fork_lookup(ingredient)
        recipes_results_str=self.search_food2fork(ingredient)
        recipes_array=self.import_from_food2fork(recipes_results_str)
        self.recipes_from_food2fork += recipes_array
        return "#{recipes_array.count} new recipes added"
    end
        
end