require 'json'
require 'recipe_recommender'
require 'open-uri'

describe RecipeRecommender do
    let(:recipe) do
        [ Recipe.new(name: 'Recipe 1'),
          Recipe.new(name: 'Recipe 2') ]
    end
    let(:recipe1) do
        [ Recipe.new(name: 'Recipe 5'),
          Recipe.new(name: 'Recipe 6') ]
    end

    let(:reciperec) { RecipeRecommender.new(recipe, recipe1) }

    

    describe "#import_recipes" do
        it 'adds new recipes' do
            data = { recipes: [ { name: 'Recipe 3'} ] }
            allow(File).to receive(:read).and_return(data.to_json)
            expect { reciperec.import_recipes('filename') }.to change { reciperec.recipes_from_file.count }.by 1
        end
    end

    describe "#export_recipes" do
        it "creates 'filename' and puts the appropriate text in it" do
            expect(File).to receive(:write).with("filename", { recipes: reciperec.recipes_from_food2fork.map(&:to_hash) }.to_json).and_return nil
            reciperec.export_recipes('filename')
        end
    end


    describe '#count_recipes' do
        it 'returns the number of recipes' do
        expect(reciperec.count_recipes).to eq("Recipes total: 2")
        end
    end

    
    describe '#import_from_food2fork' do
        let(:food2fork_text) do
            open('data/food2fork_text.json').read
        end

        let(:imported_recipes) do
            reciperec.import_from_food2fork(food2fork_text)
        end

        it 'imports correct number of recipes' do
            expect(imported_recipes.count).to eq(30)
        end

        it 'gives each recipe a title' do
            expect(imported_recipes.map(&:title).reject(&:nil?).count).to eq(30)
        end

        it 'gives each recipe a publisher' do
            expect(imported_recipes.map(&:publisher).reject(&:nil?).count).to eq(30)
        end
    end


end
