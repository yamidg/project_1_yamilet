require 'recipe'

describe Recipe do

    let(:recipe) do
        Recipe.new( title: "Bacon Wrapped Jalapeno Popper Stuffed Chicken", 
                    image_url: "http://static.food2fork.com/Bacon2BWrapped2BJalapeno2BPopper2BStuffed2BChicken2B5002B5909939b0e65.jpg", 
                    publisher: "Closet Cooking", 
                    publisher_url: "http://closetcooking.com", 
                    social_rank: 100.0, 
                    source_url: "http://www.closetcooking.com/2012/11/bacon-wrapped-jalapeno-popper-stuffed.html",
                    recipe_id: "35120")
                    
                
    end

    let(:recipe_no_information) do
        Recipe.new( title: "Buffalo Chicken Chowder",
                    image_url: "http://static.food2fork.com/Buffalo2BChicken2BChowder2B5002B0075c131caa8.jpg",
                    publisher: "All Recipes",
                    publisher_url:  "http://closetcooking.com",
                    social_rank: 100.0,
                    source_url: "http://www.closetcooking.com/2011/11/buffalo-chicken-chowder.html")
    end


    describe '#recipe_information' do
        context 'when all information is present' do
            it 'returns recipe_information' do
                expect(recipe.recipe_information).to eq("Recipe: Bacon Wrapped Jalapeno Popper Stuffed Chicken, Rank: 100.0, Publisher: Closet Cooking")
            end
        end
        context 'when not all information present' do
            it 'returns nil' do
                expect(recipe_no_information.recipe_information).to eq(nil)
            end
        end
    end
    

    describe "#to_hash" do
        
        it 'returns a hash will all instance variables' do
        
            expect(recipe.to_hash).to eq({title: "Bacon Wrapped Jalapeno Popper Stuffed Chicken",
                        image_url: "http://static.food2fork.com/Bacon2BWrapped2BJalapeno2BPopper2BStuffed2BChicken2B5002B5909939b0e65.jpg",
                        publisher: "Closet Cooking",
                        publisher_url: "http://closetcooking.com",
                        social_rank: 100.0,
                        source_url:"http://www.closetcooking.com/2012/11/bacon-wrapped-jalapeno-popper-stuffed.html", 
                        recipe_id:"35120"})
         
        end
    
    end

end